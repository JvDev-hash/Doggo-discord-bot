import requests
from bs4 import BeautifulSoup

def instagram_detect(content):

    foundedInsta = ""

    if "https://www.instagram.com/p/" in content:
        foundedInsta = "Yes"
    else:
        foundedInsta = "No"

    return foundedInsta

def instagram_webscrap(link):
    
    page = requests.get(link)

    soup = BeautifulSoup(page.text, 'html.parser')

    title = soup.find_all("script", type="application/ld+json")

    title2 = ""

    for titulo in title:
        title2 = titulo

    return str(title2)
