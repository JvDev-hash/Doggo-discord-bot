import traceback as tb     
        
        
def error_logging(exception):

    errorFile = open("logs/doggobot-error.log", "a")
    value = tb.format_tb(exception.__traceback__)
    errorFile.write("\nReturned Exception\nTraceback -- "+''.join(value)+"Exception Summary --> "+str(exception)+"\n")
    errorFile.close()

def general_logging(value):

    logging = open("logs/doggobot-general.log", "a")
    logging.write("\nGenerated log message\n\tMessage: -- "+value+"\n")
    logging.close()