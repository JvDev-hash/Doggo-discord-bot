import random
import asyncio
import io
import json
import subprocess as sb
import discord
from discord import Game
from discord.ext.commands import Bot
from discord.ext import commands
from discord import ChannelType
from modules import flushmodule as fb
from modules import youtubemodule as ytb
from modules import instagrammodule as itg
from modules import loggingmodule as logm

BOT_PREFIX = ("-")
TOKEN = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx'  # Get at discordapp.com/developers/applications/me

client = Bot(command_prefix=BOT_PREFIX)
client.remove_command('help')

paused = "No"
treatosNumber = 0

# Some functions to manipulate the global variables
def invert_values_work():
    global paused
    
    if "No" in paused:
        paused = "Yes"
    else:
        paused = "No"

def get_value_paused():
    global paused
    x = paused
    return x

# Command -dc = Bot variables configuration - Roles list init, punishment to blacklisted words 
@client.command(name='dc',
                description="Insert the settings that will be used on bot features",
                pass_context=True)
async def setter(context, *args):
    roleAuthor = context.message.author.top_role.name
    actualServer = context.message.server.name
    arrayArgs = list(args)
    test_roles = []
    found = "No"
    ok = "No"
    archiveName = actualServer + "-" + "Global" + ".json"

    try:
        with open(archiveName, 'r') as f:
            test_dict = json.load(f)
            if test_dict != None:
                test_roles = test_dict.get("value").split(", ")    
                found = "Yes"      
            else:
                pass  
        f.close()
    except sb.CalledProcessError:
        pass

    for role in test_roles:
        if roleAuthor.upper() == role.upper():
            ok = "Yes"
            break
        else:
            ok = "No"
        
        continue
    
    if found == "Yes" and ok == "Yes":
        for value in arrayArgs[0].split(", "):
            insertDict = {}
            test_roles.append(value)

            insertDict["value"] = test_roles.split(", ")
            insertDict["channel"] = "Global"

            with open(archiveName, 'w') as fp:
                json.dump(insertDict, fp)

            try:
                with open(archiveName, 'r') as f:
                    open_dict = json.load(f)
                    fb.flush_config(actualServer, open_dict)
                f.close()

            except sb.CalledProcessError:
                pass

            await client.send_message(context.message.channel, "Bork! New tricks!! :dog:")

            break

    elif found == "Yes" and ok == "No":
        await client.send_message(context.message.channel, "Wait i don't obey yuo! Bork! :dog:")

    else:
        insertDict = {}
        tempRoles = arrayArgs[0].split(", ")

        insertDict["value"] = arrayArgs[0]
        insertDict["channel"] = "AllChannels"

        with open(archiveName, 'w') as fp:
            json.dump(insertDict, fp)

        try:
            with open(archiveName, 'r') as f:
                open_dict = json.load(f)
                fb.flush_config(actualServer, open_dict)
            f.close()
        except sb.CalledProcessError:
            pass

        await client.send_message(context.message.channel, "Bork! New tricks!! :dog:")

# Command -dj = Bot interaction with jokes and tricks with him
@client.command(name='dj',
                description="Doggo interaction with jokes and tricks",
                pass_context=True)
async def jokes(context, *args):
    global treatosNumber
    actualServer = context.message.server
    command = list(args)
    messages = []
    if command[0].upper() == "catch".upper():
        await client.send_message(context.message.channel, "Bork! Oookay hooman i'll catch it for you! ~Happy running~ :dog2:")
        i = 2
        while i in range(5):
            channel= random.choice(list(actualServer.channels))
            if channel.type == ChannelType.text:
                print(channel.name)
                async for message in client.logs_from(channel, limit=100):
                    if not(message.author.bot):
                        messages.append(message)
                    else:
                        pass

                final = random.choice(messages)
                await client.send_message(context.message.channel, "Hey hooman, see wat i've found!! Bork! :dog:")    
                await client.send_message(context.message.channel, final.content)
                
                break

    elif command[0].upper() == "treato".upper():
        treatosNumber = treatosNumber+1
        ag_file = "images/catch_treato.gif"
        with open(ag_file, 'rb') as fp:
            await client.send_file(context.message.channel, fp, content = "WOOOOW!! I hardly got it the treato, thamk yuo hooman :dog:")

        await client.send_message(context.message.channel, "Treatos received = "+str(treatosNumber))
        
    elif command[0].upper() == "good".upper():
        ag_file = "images/good_boi.gif"
        with open(ag_file, 'rb') as fp:
            await client.send_file(context.message.channel, fp, content = "Am the goodest boi, thamk yuo hooman :dog: :smiling_face_with_3_hearts:")   

    elif command[0].upper() == "ball".upper():
        await client.send_message(context.message.channel, "Oookay hooman, hide the treato and i'll found it :dog:")
        await client.send_message(context.message.channel, "1st pot | 2nd pot | 3rd pot")

        numbers = [1,2,3]
        bot_choice = random.choice(numbers)

        def check(msg):
            return msg.content

        returns = await client.wait_for_message(author=context.message.author, check=check)

        if bot_choice == int(returns.content):
            await client.send_message(context.message.channel, "AWOOOO! I found it hooman :dog: Yuo owe me a treato")
        else:
            await client.send_message(context.message.channel, "Hecc! Am not found it hooman :dog:")

                    

# Command -dt = Insert the chosen channel and the tags chosen to him, because the verification will compare this tags on this and other channels        
@client.command(name='dt',
                description="Insert the chosen channel and the tags chosen to him",
                pass_context=True)
async def insert(context, *args):
    roleAuthor = context.message.author.top_role.name
    actualServer = context.message.server.name
    test_roles = []
    found = "No"
    ok = "No"
    archiveName = actualServer + "-" + "Global" + ".json"

    try:
        with open(archiveName, 'r') as f:
            test_dict = json.load(f)
            if test_dict != None:
                test_roles = test_dict.get("value").split(", ")    
                found = "Yes"      
            else:
                pass  
        f.close()
    except sb.CalledProcessError:
        pass

    for role in test_roles:
        if roleAuthor.upper() == role.upper():
            ok = "Yes"
            break
        else:
            ok = "No"
        continue

    if found == "Yes" and ok == "Yes":
        actualServer = context.message.server.name
        arrayTags = list(args)
        littleChannel = arrayTags[0]
        arrayTags.pop(0)
        mydict = {}
        keys = len(arrayTags)
        for i in range(keys):

            tag = "tag"+str(i)
            mydict[tag] = arrayTags[i]
            
        mydict["channel"] = littleChannel

        archiveName = actualServer + "-" + str(littleChannel) + ".json"
        with open(archiveName, 'w') as fp:
            json.dump(mydict, fp)

        try:
            with open(archiveName, 'r') as f:
                open_dict = json.load(f)
                fb.flush_config(actualServer, open_dict)
            f.close()

        except sb.CalledProcessError:
            pass

        sb.call("./permit.sh")
        await client.send_message(context.message.channel, "Oookay hooman {0.author.mention}, am doin a sniff snoff in this bork letters place for you :dog2: ".format(context.message))

    else:
        await client.send_message(context.message.channel, "Hooman {0.message.author.mention}, sorry i don't obey you :dog2: ".format(context))

# Command -dr = Remove an channel and tags associated to him, from the eyes of the bot
@client.command(name='dr',
                description="Remove an channel and tags associated to him, from the eyes of the bot",
                pass_context=True)
async def remover(context, canal):
    roleAuthor = context.message.author.top_role.name
    actualServer = context.message.server.name
    test_roles = []
    found = "No"
    ok = "No"
    archiveName = actualServer + "-" + "Global" + ".json"

    try:
        with open(archiveName, 'r') as f:
            test_dict = json.load(f)
            if test_dict != None:
                test_roles = test_dict.get("value").split(", ")    
                found = "Yes"      
            else:
                pass  
        f.close()
    except sb.CalledProcessError:
        pass

    for role in test_roles:
        if roleAuthor.upper() == role.upper():
            ok = "Yes"
            break
        else:
            ok = "No"
        continue

    if found == "Yes" and ok == "Yes":
        actualServer = context.message.server.name
        argument = actualServer + "-" + canal + ".json"
        sb.call(["./remove.sh", argument])
        await client.send_message(context.message.channel, "Sniff.. Oookay hooman.. am doin a leave that bork letters place :dog2: :cry:")
        
    else:
        await client.send_message(context.message.channel, "Hooman {0.message.author.mention}, sorry i don't obey you :dog2: ".format(context))

# Command -dh = List of commands aka Help command
@client.command(name='dh',
                description="Custom Help command",
                pass_context=True)
async def halper(context):
    author = context.message.author

    embed = discord.Embed(
        colour = discord.Colour.orange()
    )

    embed.set_author(name = '[Doggo Halp]')

    jsonName = "modules/commands.json"
    with open(jsonName, 'r') as f:
        response = json.load(f)

    keys = len(response["commands"])
    for i in range(keys):

        tag = "tag"+str(i)
        embed.add_field(name = str(response["commands"][tag])[0:3], value = str(response["commands"][tag])[6:], inline = False)
        
    embed.add_field(name = "Extra Infos", value=str(response.get("extraInfos")), inline = False)

    await client.send_message(context.message.channel, " :dog: Bork!! Oookay hooman, am doin a send the list of my tricks.. :dog2: ".format(context))
    await client.send_message(author, embed=embed)

# Command -dp = Pause the bot work, for a moment
@client.command(name='dp',
                description="Pause the Doggo Patrol",
                pass_context=True)
async def pauser(context):
    x = get_value_paused()

    roleAuthor = context.message.author.top_role.name
    actualServer = context.message.server.name
    test_roles = []
    found = "No"
    ok = "No"
    archiveName = actualServer + "-" + "Global" + ".json"

    try:
        with open(archiveName, 'r') as f:
            test_dict = json.load(f)
            if test_dict != None:
                test_roles = test_dict.get("value").split(", ")    
                found = "Yes"      
            else:
                pass  
        f.close()
    except sb.CalledProcessError:
        pass

    for role in test_roles:
        if roleAuthor.upper() == role.upper():
            ok = "Yes"
            break
        else:
            ok = "No"
        continue

    if 'No' == x and found == "Yes" and ok == "Yes":
        invert_values_work()
        await client.send_message(context.message.channel, "Ok fren {0.message.author.mention}, i'll relax for now! :dog:".format(context))
    elif 'Yes' == x and found == "Yes" and ok == "Yes":
        invert_values_work()
        await client.send_message(context.message.channel, "Ok fren {0.message.author.mention}, i'm goin to work! :dog2:".format(context))
    else:
        await client.send_message(context.message.channel, "Hooman {0.message.author.mention}, sorry i don't obey you :dog2: ".format(context))

# Command -dat = Insert the tags chosen are, banned on the server because the verification will compare this tags
@client.command(name='dat',
                description="Insert the tags chosen to him",
                pass_context=True)
async def advanced(context, *args):
    roleAuthor = context.message.author.top_role.name
    actualServer = context.message.server.name
    test_roles = []
    found = "No"
    ok = "No"
    archiveName = actualServer + "-" + "AllChannels" + ".json"
    roleArchive = actualServer + "-" + "Global" + ".json"

    try:
        with open(roleArchive, 'r') as f:
            test_dict = json.load(f)
            if test_dict != None:
                test_roles = test_dict.get("value").split(", ")    
                found = "Yes"      
            else:
                pass  
        f.close()
    except sb.CalledProcessError:
        pass

    for role in test_roles:
        if roleAuthor.upper() == role.upper():
            ok = "Yes"
            break
        else:
            ok = "No"
        continue

    if found == "Yes" and ok == "Yes":
        actualServer = context.message.server.name
        arrayTags = list(args)
        mydict = {}
        keys = len(arrayTags)
        for i in range(keys):

            tag = "tag"+str(i)
            mydict[tag] = arrayTags[i]

        mydict["channel"] = "AllChannels"

        with open(archiveName, 'w') as fp:
            json.dump(mydict, fp)

        try:
            with open(archiveName, 'r') as f:
                open_dict = json.load(f)
                fb.flush_config(actualServer, open_dict)
            f.close()

        except sb.CalledProcessError:
            pass

        sb.call("./permit.sh")
        await client.send_message(context.message.channel, "Oookay hooman {0.author.mention}, am doin a sniff snoff in this borks for you :dog2: ".format(context.message))
    else:
        await client.send_message(context.message.channel, "Hooman {0.message.author.mention}, sorry i don't obey you :dog2: ".format(context))

# Command -ds = Show the Bot status    
@client.command(name='ds',
                description="Show the status of the Doggo Patrol",
                pass_context=True)
async def statuser(context):
    x = get_value_paused()
    archiveName = ""

    if 'Yes' == x:
        archiveName = "images/doggo_sleep.jpg"
        with open(archiveName, 'rb') as fp:
            await client.send_file(context.message.channel, fp, content = "Heey fren {0.message.author.mention}, i'm relaxin now! :dog:".format(context))
        fp.close()
    elif 'No' == x:
        archiveName = "images/doggo_work.jpg"
        with open(archiveName, 'rb') as fp:
            await client.send_file(context.message.channel, fp, content = "I'm doin works now :dog: :dog2:")
        fp.close()

# Command -dl = Lists which text channels are registered on the bot
@client.command(name='dl',
                description="Lists which text channels are registered on the bot",
                pass_context=True)
async def lister(context):
    actualServer = context.message.server
    try:
        outputChannels = sb.check_output(["./list.sh", actualServer.name]).decode("utf-8").split("\n")
        outputChannels.remove("")
        finalOutput = ""
        for channel in actualServer.channels:
            for outputs in outputChannels:
                if channel.name in outputs:
                    finalOutput += "\n" + channel.name

        await client.send_message(context.message.channel, "Hooman {0.message.author.mention}, here the list of channels that i sniff snoff :dog2: ".format(context))
        await client.send_message(context.message.channel, finalOutput)
        print(context.message.author.top_role)

    except sb.CalledProcessError:
        raise
        await client.send_message(context.message.channel, "Hooman i'dont have any channel recorded! :dog:")

# Command -df = Load configs from database
@client.command(name='df',
                description="Load configs from database",
                pass_context=True)
async def flusher(context):
    roleAuthor = context.message.author.top_role.name
    actualServer = context.message.server.name
    test_roles = []
    found = "No"
    ok = "No"
    archiveName = actualServer + "-" + "Global" + ".json"

    try:
        with open(archiveName, 'r') as f:
            test_dict = json.load(f)
            if test_dict != None:
                test_roles = test_dict.get("value").split(", ")    
                found = "Yes"      
            else:
                pass  
        f.close()
    except sb.CalledProcessError:
        pass

    for role in test_roles:
        if roleAuthor.upper() == role.upper():
            ok = "Yes"
            break
        else:
            ok = "No"
        continue

    if found == "Yes" and ok == "Yes":
        await client.send_message(context.message.channel, "Ok! Hooman i'll get my doggo toys :dog:")
        try:
            for chanels in actualServer.channels:
                if chanels.type == ChannelType.text:
                    open_dict = fb.get_config(actualServer.name, chanels.name)
                    archiveName = actualServer.name + "-" + chanels.name + ".json"
                    if open_dict != None:
                        with open(archiveName, 'w') as fp:
                            json.dump(open_dict, fp)

        except sb.CalledProcessError:
            pass

    else:
        await client.send_message(context.message.channel, "Hooman {0.message.author.mention}, sorry i don't obey you :dog2: ".format(context))

# Error handler
@client.event
async def on_command_error(exception, context):

    if isinstance(exception, commands.CommandNotFound):
        await client.send_message(context.message.channel, "~No understaning face~ :thinking: Am no undestand wat yuo doin hooman.. Try again")
        logm.error_logging(exception)

    elif isinstance(exception, commands.MissingRequiredArgument):
        await client.send_message(context.message.channel, ":dog: You forgot something on this command hooman.. Try again")
        logm.error_logging(exception)

    else:
        logm.error_logging(exception)
        pass

# Core feature of the bot
@client.event
async def on_message(message):

    if get_value_paused() == 'No':
        # 'PrivateChannel' object has no attribute 'server'
        if message.channel.type == ChannelType.private:
            return

        founded = "No"

        extrasContent = ""
        
        probs = ""

        # Getting the actual server and all channels on him
        actualServer = message.channel.server
        todos_canal = []
        i = 0
        for canal in actualServer.channels:
            if canal.type == ChannelType.text:
                todos_canal.insert(i, canal.id)
                i = i + 1
        
        probWords = []
        wordsName = actualServer.name + "-" + "AllChannels" + ".json"
        try:
            with open(wordsName, 'r') as f:
                words_dict = json.load(f)
                wtags = len(words_dict) - 1
                for i in range(wtags):
                    newTags = "tag"+str(i)
                    probWords.append(words_dict.get(newTags))
            f.close()

        except sb.CalledProcessError:
            pass
        # We do not want the bot to reply to itself
        if message.author == client.user:
            return
        
        # If the message is a command, process as a command
        if message.content.startswith("-d"):
            await client.process_commands(message)

        else:
            try:
                # If the message is a Youtube Link or a Instagram Link, web scrap the infos
                if ytb.youtube_detect(message.content) == "Yes":
                    extrasContent = ytb.youtube_webscrap(message.content)

                elif itg.instagram_detect(message.content) == "Yes":
                    extrasContent = itg.instagram_webscrap(message.content)
                else:
                    pass

                # If the message have white spaces, remove then
                if ' ' in message.content:
                    tempContent = message.content
                    extrasContent = tempContent.replace(' ', '')

                else:
                    pass
                
                # Verification if the word are on the blacklisted words before the verification of wrong channel
                for probs in probWords:
                    if message.content != "" and message.content.replace(" ", "").upper() in probs.replace(" ", "").upper():
                        msg = "AWOOOOO! :dog: Hey fren {0.author.mention}, yuo cannot bork this here: {0.channel.server}".format(message)
                        founded = "Yes"
                        
                        await client.send_message(message.channel, msg)
                        await client.delete_message(message)

                        break
                    else:
                        pass

                # Verification if the message contains a word that in the wrong channel, and moving her to the right place
                for canal in actualServer.channels:
                    if canal.type == ChannelType.text:
                        jsonName = actualServer.name + "-" + str(canal.name) + ".json"
                        outputChannels = sb.check_output(["./list.sh", actualServer.name]).decode("utf-8").split("\n")
                        outputChannels.remove("")
                        if jsonName in outputChannels and founded != "Yes":
                            with open(jsonName, 'r') as f:
                                open_dict = json.load(f)
                                idTags = len(open_dict) - 1
                                for i in range(idTags):
                                    newTags = "tag"+str(i)
                                    dictValue = open_dict.get(newTags)
                                    if dictValue.replace(" ", "").upper() in message.content.replace(" ", "").upper() and message.channel.name != open_dict.get("channel"):
                                        msg = "AWOOOOO! :dog: Hey fren {0.author.mention}, your bork doesn't belongs here: {0.channel}".format(message)
                                        sending = "Bork! :dog: The hooman {0.author.mention} borked this on wrong place".format(message)
                                        founded = "Yes"
                                        
                                        await client.send_message(message.channel, msg)
                                        await client.delete_message(message)
                                        await client.send_message(client.get_channel(canal.id), sending)
                                        await client.send_message(client.get_channel(canal.id), message.content)
                                        

                                        break

                                    elif dictValue.replace(" ", "").upper() in extrasContent.replace(" ", "").upper() and message.channel.name != open_dict.get("channel"):
                                        msg = "AWOOOOO! :dog: Hey fren {0.author.mention}, your bork doesn't belongs here: {0.channel}".format(message)
                                        sending = "Bork! :dog: The hooman {0.author.mention} borked this on wrong place".format(message)
                                        founded = "Yes"
                                        
                                        await client.send_message(message.channel, msg)
                                        await client.delete_message(message)
                                        await client.send_message(client.get_channel(canal.id), sending)
                                        await client.send_message(client.get_channel(canal.id), message.content)
                                        

                                        break
                                    
                                    else:
                                        pass
                        else:
                            pass 
                                        
                    if founded == "Yes":
                        break
                    else:
                        continue

            except sb.CalledProcessError:
                return
    else:
        await client.process_commands(message)
    
# When the bot exec the on_ready, he'll load the configs from database
@client.event
async def on_ready():
    for server in client.servers:
        for chanels in server.channels:
            if chanels.type == ChannelType.text:
                open_dict = fb.get_config(server.name, chanels.name)
                archiveName = server.name + "-" + chanels.name + ".json"
                if open_dict != None:
                    with open(archiveName, 'w') as fp:
                        json.dump(open_dict, fp)
                    
        config_dict = fb.get_config(server.name, "Global")
        config_archive = server.name + "-" + "Global" + ".json"
        if config_dict != None:
            with open(config_archive, 'w') as fp:
                json.dump(config_dict, fp)
            
        bwords_dict = fb.get_config(server.name, "AllChannels")
        bwords_archive = server.name + "-" + "AllChannels" + ".json"
        if bwords_dict != None:
            with open(bwords_archive, 'w') as fp:
                json.dump(bwords_dict, fp)

    logm.general_logging("Logged in as " + client.user.name)
    
async def list_servers():
    await client.wait_until_ready()
    while not client.is_closed:
        print("Current servers:")
        for server in client.servers:
            print(server.name)
        await asyncio.sleep(900)

# Looping to change the status
async def status_task():
    await client.wait_until_ready()
    while True:
        await client.change_presence(game=Game(name="For my tricks, bork -dh", type=0))
        await asyncio.sleep(50)
        await client.change_presence(game=Game(name="Doin a sniff snoff on hoomans", type=0))
        await asyncio.sleep(50)

client.loop.create_task(list_servers())
client.loop.create_task(status_task())
client.run(TOKEN)
